DROP TABLE IF EXISTS `plugin_googlesheets_sheets`;
CREATE TABLE `plugin_googlesheets_sheets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sheet_key` text COLLATE utf8_unicode_ci NOT NULL,
  `species` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

