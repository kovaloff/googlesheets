<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 12/30/14
 * Time: 6:29 PM
 */
class Api_Googlesheets_GS extends Api_Service_Abstract {
    public function postAction() {
        $content = $this->_request->getParam('content');
        $name    = filter_var($this->_request->getParam('containerName'), FILTER_SANITIZE_STRING);
        $pageId  = filter_var($this->_request->getParam('pageId'), FILTER_SANITIZE_NUMBER_INT);
        $type    = ($pageId) ? Application_Model_Models_Container::TYPE_REGULARCONTENT : Application_Model_Models_Container::TYPE_STATICCONTENT;

        if($pageId == 0) {
            $pageId = null;
        }

        $mapper    = Application_Model_Mappers_ContainerMapper::getInstance();
        $container = $mapper->findByName($name, $pageId, $type);
        if(!$container instanceof Application_Model_Models_Container) {
            $container = new Application_Model_Models_Container();
            $container->setPageId($pageId)
                ->setContainerType($type)
                ->setName($name);
        }
        $container->setContent($content);

        try {
            return array('error' => false, 'responseText' => $mapper->save($container));
        } catch (Exception $e) {
            return $this->_error($e->getMessage());
        }
    }

    public function getAction() {}
    public function putAction() {}
    public function deleteAction() {}
}