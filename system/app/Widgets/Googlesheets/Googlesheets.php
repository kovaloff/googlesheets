<?php

/**
 * Created by PhpStorm.
 * User: artem
 * Date: 12/30/14
 * Time: 12:57 PM
 */
class Widgets_Googlesheets_Googlesheets extends Widgets_Abstract
{
    protected function _init()
    {
        parent::_init();
        // initialize layout
        $this->_view = new Zend_View();
        if (($scriptPaths = Zend_Layout::getMvcInstance()->getView()->getScriptPaths()) !== false) {
            $this->_view->setScriptPath($scriptPaths);
        }
        $this->_view->addScriptPath(__DIR__ . '/views/');
        $this->_websiteHelper = Zend_Controller_Action_HelperBroker::getStaticHelper('website');
        $this->_mapper = Application_Model_Mappers_ContainerMapper::getInstance();
        $this->_sheetselect = $sheetSelect = new Googlesheets_Forms_SheetSelect();
        $this->_sheetDb = new Googlesheets_Models_DbTable_Googlesheets();

    }

    protected function _load()
    {
        if (empty($this->_options[0]) || empty($this->_options[1]) || empty($this->_options[2]) || empty($this->_options[3])) {
            throw new Exceptions_SeotoasterPluginException('Not enough parameters passed!');
        }
        $option = strtolower($this->_options[3]);
        $renderer = '_render' . ucfirst($option);
        if (method_exists($this, $renderer)) {
            return $this->$renderer();
        }
    }

    public function _renderMap()
    {
        $mapWidth = '580';
        $mapHeight = '350';

        if (!empty($this->_options[0])) {
            $containerName = $this->_options[0];
            $this->_view->containerName = $this->_options[0];
        }
        if (!empty($this->_options[1])) {
            $mapWidth = $this->_options[1];
        }
        if (!empty($this->_options[2])) {
            $mapHeight = $this->_options[2];
        }

        $this->_view->mapWidth = $mapWidth;
        $this->_view->mapHeight = $mapHeight;

        $pageId = filter_var($this->_toasterOptions['id'], FILTER_SANITIZE_NUMBER_INT);
        $containerName = filter_var($this->_options[0], FILTER_SANITIZE_STRING);

        $type = ($pageId) ? Application_Model_Models_Container::TYPE_REGULARCONTENT : Application_Model_Models_Container::TYPE_STATICCONTENT;
        $container = $this->_mapper->findByName($containerName, $pageId, $type);

        $where = $this->_sheetDb->getAdapter()->quoteInto('species IN(?)', 'map');
        $sheets = $this->_sheetDb->getAdapter()->fetchAll(
            $this->_sheetDb->getAdapter()->select()->from(
                'plugin_googlesheets_sheets',
                array('sheet_key', 'name')
            )->where($where)
        );
        foreach ($sheets as $sheet) {
            $this->_sheetselect->getElement('sheetKey')->addMultiOption($sheet['sheet_key'], $sheet['name']);
        }
        if ($container instanceof Application_Model_Models_Container) {
            $sheetKey = json_decode($container->content, true);
            if ($pageId == 0) {
                $pageId = null;
            }
            $container = new Application_Model_Models_Container();
            $container->setName($containerName)
                ->setPageId($pageId)
                ->setContainerType(
                    ($pageId) ? Application_Model_Models_Container::TYPE_REGULARCONTENT : Application_Model_Models_Container::TYPE_STATICCONTENT
                );
            $this->_view->sheets = $sheetKey;
            $this->_sheetselect->setDefault('sheetKey', $sheetKey['sheet_key']);
        }
        $this->_view->form = $this->_sheetselect;
        $this->_view->containerName = $containerName;
        $this->_view->pageId = $pageId;
        return $this->_view->render('mapsheet.phtml');
    }

    public function _renderTable()
    {
        $Width = '580';
        $Height = '350';

        if (!empty($this->_options[0])) {
            $containerName = $this->_options[0];
            $this->_view->containerName = $this->_options[0];
        }
        if (!empty($this->_options[1])) {
            $Width = $this->_options[1];
        }
        if (!empty($this->_options[2])) {
            $Height = $this->_options[2];
        }

        $this->_view->mapWidth = $Width;
        $this->_view->mapHeight = $Height;

        $pageId = filter_var($this->_toasterOptions['id'], FILTER_SANITIZE_NUMBER_INT);
        $containerName = filter_var($this->_options[0], FILTER_SANITIZE_STRING);

        $type = ($pageId) ? Application_Model_Models_Container::TYPE_REGULARCONTENT : Application_Model_Models_Container::TYPE_STATICCONTENT;
        $container = $this->_mapper->findByName($containerName, $pageId, $type);

        $where = $this->_sheetDb->getAdapter()->quoteInto('species IN(?)', 'table');
        $sheets = $this->_sheetDb->getAdapter()->fetchAll(
            $this->_sheetDb->getAdapter()->select()->from(
                'plugin_googlesheets_sheets',
                array('sheet_key', 'name')
            )->where($where)
        );
        foreach ($sheets as $sheet) {
            $this->_sheetselect->getElement('sheetKey')->addMultiOption($sheet['sheet_key'], $sheet['name']);
        }
        if ($container instanceof Application_Model_Models_Container) {
            $sheetKey = json_decode($container->content, true);
            if ($pageId == 0) {
                $pageId = null;
            }
            $container = new Application_Model_Models_Container();
            $container->setName($containerName)
                ->setPageId($pageId)
                ->setContainerType(
                    ($pageId) ? Application_Model_Models_Container::TYPE_REGULARCONTENT : Application_Model_Models_Container::TYPE_STATICCONTENT
                );
            $this->_view->table = $sheetKey;
            $this->_sheetselect->setDefault('sheetKey', $sheetKey['sheet_key']);
        }
        $this->_view->form = $this->_sheetselect;
        $this->_view->containerName = $containerName;
        $this->_view->pageId = $pageId;
        return $this->_view->render('tablesheet.phtml');
    }
}