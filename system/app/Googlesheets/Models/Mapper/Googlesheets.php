<?php

/**
 * Googlesheets Mapper
 *
 */
class Googlesheets_Models_Mapper_Googlesheets extends Application_Model_Mappers_Abstract
{

    protected $_dbTable = 'Googlesheets_Models_DbTable_Googlesheets';

    protected $_model = 'Googlesheets_Models_Model_Googlesheets';

    public function save($sheet)
    {
        $data = array(
            'name' => $sheet->getName(),
            'sheet_key' => $sheet->getSheetKey(),
            'species' => $sheet->getSpecies()
        );

        if (!$this->findByKey($sheet->getSheetKey()) && !$this->findByName($sheet->getName())) {
            return $this->getDbTable()->insert($data);
        }
        return false;
    }
    public function findByKey($sheetKey) {
        $where = $this->getDbTable()->getAdapter()->quoteInto('sheet_key=?', $sheetKey);
        return $this->_findWhere($where);
    }
    public function findByName($name) {
        $where = $this->getDbTable()->getAdapter()->quoteInto('name=?', $name);
        return $this->_findWhere($where);
    }

    public function findBySpecies($species) {
        $where = $this->getDbTable()->getAdapter()->quoteInto('species=?', $species);
        return $this->_findWhere($where);
    }
    public function delete(Googlesheets_Models_Model_Googlesheets $sheet) {
        $where = $this->getDbTable()->getAdapter()->quoteInto('id = ?', $sheet->getId());
        $deleteResult = $this->getDbTable()->delete($where);
        $sheet->notifyObservers();
    }

}

