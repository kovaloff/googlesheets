<?php

/**
 * Googlesheets Model
 *
 */
class Googlesheets_Models_Model_Googlesheets extends Application_Model_Models_Abstract
{


    protected $_name = '';

    protected $_species = '';

    protected $_sheetKey = '';

    protected $_id = '';


    public function getName()
    {
        return $this->_name;
    }

    public function setName($name)
    {
        $this->_name = $name;
        return $this;
    }

    public function getSpecies()
    {
        return $this->_species;
    }

    public function setSpecies($species)
    {
        $this->_species = $species;
        return $this;
    }

    public function getSheetKey()
    {
        return $this->_sheetKey;
    }

    public function setSheetKey($sheet_key)
    {
        $this->_sheetKey = $sheet_key;
        return $this;
    }

    public function getId()
    {
        return $this->_id;
    }


}

