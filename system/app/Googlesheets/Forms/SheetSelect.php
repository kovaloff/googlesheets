<?php

/**
 * Created by PhpStorm.
 * User: artem
 * Date: 12/26/14
 * Time: 1:48 PM
 */
class Googlesheets_Forms_SheetSelect extends Zend_Form
{

    protected $_sheetsKey = '';


    public function init()
    {

        $this->setMethod(Zend_Form::METHOD_POST)
            ->setName('sheetForm')
            ->setAttrib('class', '_fajax');

        $this->addElement(
            new Zend_Form_Element_Select(array(
                'id' => 'sheetKey',
                'name' => 'sheetKey',
                'class' => 'grid_6'
            ))
        );

        $this->addElement(
            new Zend_Form_Element_Button(array(
                'name' => 'selectSheets',
                'id' => 'selectSheets',
                'value' => 'selectSheets',
                'class' => 'btn grid_2 omega',
                'label' => 'Save',
                'type' => 'submit'
            ))
        );

        $this->setElementDecorators(array('ViewHelper', 'Label'));

    }

    public function getSheets()
    {
        return $this->_sheetsKey;
    }

    public function setSheets($sheets)
    {
        $this->__sheetsKey = $sheets;
        $this->getElement('sheets')->setValue($sheets);
        return $this;
    }


}
