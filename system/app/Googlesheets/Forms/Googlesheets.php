<?php

/**
 * Created by PhpStorm.
 * User: artem
 * Date: 12/26/14
 * Time: 1:48 PM
 */
class Googlesheets_Forms_Googlesheets extends Zend_Form
{

    protected $_sheetsKey = '';

    protected $_sheetName = '';

    public function init()
    {

        $this->setMethod(Zend_Form::METHOD_POST)
            ->setAttrib('class', '_fajax')
            ->setAttrib('data-callback', 'reloadSheetsList');

        $this->addElement(
            new Zend_Form_Element_Text(array(
                'id' => 'sheetsKey',
                'name' => 'sheetsKey',
                'label' => 'Add sheets',
                'required' => true,
            ))
        );

        $this->addElement(
            new Zend_Form_Element_Text(array(
                'id' => 'sheetsName',
                'name' => 'sheetsName',
                'label' => 'Sheet Name',
                'required' => true,
            ))
        );

        $this->addElement(
            new Zend_Form_Element_Select(array(
                'id' => 'sheetKind',
                'name' => 'sheetKind',
                'label' => 'Species',
            ))
        );

        $this->addElement(
            new Zend_Form_Element_Button(array(
                'name' => 'addSheets',
                'id' => 'addSheets',
                'value' => 'addSheets',
                'class' => 'btn ticon-plus grid_2 omega',
                'label' => 'Add sheets',
                'type' => 'submit'
            ))
        );

        $this->setElementDecorators(array('ViewHelper', 'Label'));

    }

    public function getSheets()
    {
        return $this->_sheetsKey;
    }

    public function setSheets($sheets)
    {
        $this->__sheetsKey = $sheets;
        $this->getElement('sheets')->setValue($sheets);
        return $this;
    }

    public function getsheetName()
    {
        return $this->_sheetName;
    }

    public function setsheetName($sheetName)
    {
        $this->_sheetName = $sheetName;
        $this->getElement('sheetName')->setValue($sheetName);
        return $this;
    }

}

