$(function () {
    reloadSheetsList();
    $('.redirect-massdel').on('click', function () {
        if (!$('.redirect-massdel').not(':checked').length) {
            $('#massdell-main').attr('checked', true);
        }
        else {
            $('#massdell-main').attr('checked', false);
        }
    });

    $('#massdell-main').click(function () {
        $('.redirect-massdel').prop('checked', ($(this).prop('checked')) ? true : false);
    });

    $('#massdel-run').click(function () {
        var ids = [];
        $('.redirect-massdel:checked').each(function () {
            ids.push($(this).attr('id'));
        });
        if (!ids.length) {
            showMessage('Select at least one item, please', true);
            return false;
        }
        showConfirm('You are about to remove one or many redirects. Are you sure?', function () {
            var callback = $('#frm-sheets').data('callback');
            $.ajax({
                url: $('#website_url').val() + '/plugin/googlesheets/run/removeSheets/',
                type: 'post',
                data: {
                    id: ids
                },
                dataType: 'json',
                beforeSend: function () {
                    showSpinner();
                },
                success: function (response) {
                    hideSpinner();
                    showMessage(response.responseText, response.error);
                    if (typeof callback != 'undefined') {
                        eval(callback + '()');
                    }
                }
            });
        });
    })
});

//callback function for the ajax forms
function reloadSheetsList() {
    showSpinner();
    $.getJSON($('#website_url').val() + 'plugin/googlesheets/run/loadSheetsList/', function (response) {
        hideSpinner();
        $('#sheets-list').html(response.responseText);
        checkboxRadioStyle();
    });
}