<?php

class Googlesheets extends Tools_Plugins_Abstract
{
    protected $_cacheable = false;

    protected $_layout = null;

    protected $_configHelper = null;

    protected function _init()
    {
        // initialize layout
        $this->_layout = new Zend_Layout();
        $this->_layout->setLayoutPath(Zend_Layout::getMvcInstance()->getLayoutPath());

        if (($scriptPaths = Zend_Layout::getMvcInstance()->getView()->getScriptPaths()) !== false) {
            $this->_view->setScriptPath($scriptPaths);
        }
        $this->_view->addScriptPath(__DIR__ . '/views/');
        $this->_configHelper = Zend_Controller_Action_HelperBroker::getStaticHelper('config');
        $ajaxHelper = Zend_Controller_Action_HelperBroker::getStaticHelper('AjaxContext');
        $ajaxHelper->addActionContexts(
            array(
                'loadSheetsList' => 'json'
            )
        )->initContext('json');
        $this->_mapper = Application_Model_Mappers_ContainerMapper::getInstance();
    }

    public function addSheetsAction()
    {
        if (Tools_Security_Acl::isAllowed(Tools_Security_Acl::RESOURCE_PLUGINS)) {
            $species = array('map'=>'map','table'=>'table');
            $form = new Googlesheets_Forms_Googlesheets();
            foreach ($species as $specie) {
            $form->getElement('sheetKind')->addMultiOption($specie,$specie);
            }
                $sheetMapper = Googlesheets_Models_Mapper_Googlesheets::getInstance();
            if ($this->_request->isPost()) {
                $data = $this->_request->getParams();
                if (!empty($data['sheetsName']) && !empty($data['sheetsKey'])) {
                    $sheets = new Googlesheets_Models_Model_Googlesheets();
                    $sheets->setName($data['sheetsName']);
                    $sheets->setSheetKey($data['sheetsKey']);
                    $sheets->setSpecies($data['sheetKind']);
                    $result = $sheetMapper->save($sheets);
                    if ($result) {
                        $this->_responseHelper->success('Sheets added');
                    } else {
                        $this->_responseHelper->fail('Such sheet already exists');
                    }
                } else {
                    $this->_responseHelper->fail('Fill all required fields');
                }
            }

            $this->_view->form = $form;
            $this->_layout->content = $this->_view->render('sheets.phtml');
            echo $this->_layout->render();
        }
    }

    public function removeSheetsAction()
    {
        if ($this->_request->isPost()) {
            $ids = $this->_request->getParam('id');
            $sheetMapper = Googlesheets_Models_Mapper_Googlesheets::getInstance();
            if (is_array($ids)) {
                foreach ($ids as $id) {
                    $sheetMapper->delete($sheetMapper->find($id));
                }
            } else {
                $sheetMapper->delete($sheetMapper->find($ids));
            }

            $this->_responseHelper->success('Sheet(s) removed.');
        }
    }

    public function loadSheetsListAction()
    {
        $sheets = Googlesheets_Models_Mapper_Googlesheets::getInstance()->fetchall(null, array('id'));
        $this->_view->sheets = array_reverse($sheets);
        $this->_layout->sheetslist = $this->_view->render('loadsheetslist.phtml');
        $this->_responseHelper->response($this->_view->render('loadsheetslist.phtml'));
        echo $this->_layout->render();
    }

    public function savesheetsAction()
    {
        $data = $this->_request->getParam('containerName');
        $pieces = explode("/", $data);
        $containerName = $pieces[0];
        $pageId = $pieces[1];
        $content = $this->_request->getParam('sheetKey');
        $name = filter_var($containerName, FILTER_SANITIZE_STRING);
        $type = ($pageId) ? Application_Model_Models_Container::TYPE_REGULARCONTENT : Application_Model_Models_Container::TYPE_STATICCONTENT;
        $container = $this->_mapper->findByName($containerName, $pageId, $type);
        $sheetData = array(
            'sheet_key' => filter_var($content, FILTER_SANITIZE_STRING),
        );
        if ($pageId == 0) {
            $pageId = null;
        }

        if (!$container instanceof Application_Model_Models_Container) {
            $container = new Application_Model_Models_Container();
            $container->setName($containerName)
                ->setPageId($pageId)
                ->setContainerType(
                    ($pageId) ? Application_Model_Models_Container::TYPE_REGULARCONTENT : Application_Model_Models_Container::TYPE_STATICCONTENT
                );
        }
        $container->setContent(Zend_Json::encode($sheetData));

        if ($this->_mapper->save($container)) {
            $this->_responseHelper->success('Sheet saved');
        } else {
            $this->_responseHelper->fail("Already saved");
        }
    }
}